import { buildResponseError, buildResponseSuccess } from '../../utils/utils.js';
import { RES_STATUS } from '../../utils/constants.js'
import  logger from '../../config/winston.js';
import {registerUser} from '../../services/au/registerService.js';
import {validationResult} from 'express-validator';

export const registerController = async (req, res) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            const response = buildResponseError(RES_STATUS.STATUS.ERROR, RES_STATUS.CODE.CODE_101, errors.array());
            return res.status(RES_STATUS.STATUS.ERROR).send(response);
        }

        const {name, email, phone, password} = req.body;
        await registerUser(name, email, phone, password);
        res.send(buildResponseSuccess());
    } catch (e) {
        logger.error(e.message);
        res.send(buildResponseError(RES_STATUS.STATUS.SERVER_ERROR));
    }
}