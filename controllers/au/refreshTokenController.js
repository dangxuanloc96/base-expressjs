import { buildResponseError, buildResponseSuccess } from '../../utils/utils.js';
import { RES_STATUS } from '../../utils/constants.js'
import  logger from '../../config/winston.js';
import  { refreshTokenService } from '../../services/au/refreshTokenService.js'

export const refreshController = async (req, res) => {
    try {
        const refreshToken = req.body.refresh_token;

        const data = refreshTokenService(refreshToken);
        res.send(buildResponseSuccess(data));
    } catch (e) {
        logger.error(e.message);
        res.send(buildResponseError(RES_STATUS.STATUS.SERVER_ERROR));
    }
}