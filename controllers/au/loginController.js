import { buildResponseError, buildResponseSuccess } from '../../utils/utils.js';
import { RES_STATUS } from '../../utils/constants.js'
import  logger from '../../config/winston.js';
import {loginService} from '../../services/au/loginService.js';

export const loginController = async (req, res) => {
    try {
        const email = req.body.email;
        const password = req.body.password;
        const data = await loginService(email, password);
        const response = buildResponseSuccess(data);
        res.send(response);
    } catch (e) {
        logger.error(e.message);
        res.send(buildResponseError(RES_STATUS.STATUS.SERVER_ERROR));
    }
}