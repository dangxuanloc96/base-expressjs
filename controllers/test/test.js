import { buildResponseSuccess } from '../../utils/utils.js';

export const test = async (req, res) => {
    const {user_id, name, email, phone} = req.infoUser;
    const response = buildResponseSuccess({user_id, name, email, phone}, "Thông tin của người dùng đang đăng nhập");
    res.send(response);
}