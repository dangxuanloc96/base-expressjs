import chai from '../chai.js';
import app from '../../app.js';
const expect = chai.expect;

const body = {
    "name": "DangGiaHan",
    "email": "danggiahan2021@gmail.com",
    "phone": "0374380030",
    "password": "Test1002",
    "password_confirm": "Test1002"
}

// Nên có những Body data test hết các trạng thái của api
// 200: success
// 400: validate

// Đăng kí tài khoản
describe('POST v1/au/register', () => {
    it('should return status 200', (done) => {
        chai
            .request(app)
            .post('/v1/au/register')
            .set('Cookie', 'Jsessionid=gsgUyncd22fYhnlgaganlnnjdn9GYnmgsa')
            .set('User-Agent', 'Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41')
            .set('X-Caller-App-Id', 'registration_003_000')
            .set('X-Channel-Type', 'netbksmk_web')
            .send(body)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.an('object');
                res.body.should.have.property('messages');
                const messages = res.body.messages;
                expect(messages).to.be.an('array');
                messages.forEach((item) => {
                    item.should.have.property('id');
                    item.should.have.property('message');
                });
                done();
            });
    });
});
