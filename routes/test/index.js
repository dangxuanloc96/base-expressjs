import express from 'express';
import {testRoutes} from './test.js';
import {auth} from '../../utils/middleware/authMiddleware.js'

const TESTRouter = express.Router();
TESTRouter.use(auth, testRoutes);

export default TESTRouter;

