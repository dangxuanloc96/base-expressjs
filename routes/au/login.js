import express from 'express';
export const loginRoutes = express.Router();
import { loginController } from '../../controllers/au/loginController.js';
import { RESTURL } from '../../utils/constants.js';

// Đăng nhập
loginRoutes.post(RESTURL.PATH.LOGIN, loginController);