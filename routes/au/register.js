import express from 'express';
export const registerRoutes = express.Router();
import { registerController } from '../../controllers/au/registerController.js';
import { RESTURL } from '../../utils/constants.js';
import {check} from "express-validator";
import {ERROR_CODE} from "../../config/error.js";
const regexPhoneNumber = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g
const regexPassword = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{8})$/g

// Đăng kí
registerRoutes.post(RESTURL.PATH.REGISTER,
    check('name').trim().notEmpty().withMessage(ERROR_CODE.code1),
    check('email').trim().notEmpty().withMessage(ERROR_CODE.code1)
        .if(check('email').notEmpty()).isEmail().withMessage(ERROR_CODE.code2),
    check('phone').trim().notEmpty()
        .trim().notEmpty().withMessage(ERROR_CODE.code1)
        .if(check('phone').notEmpty()).matches(regexPhoneNumber).withMessage(ERROR_CODE.code2),
    check('password_confirm').trim().notEmpty().withMessage(ERROR_CODE.code1)
        .if(check('password_confirm').notEmpty()).matches(regexPassword).withMessage(ERROR_CODE.code2),
    check('password').trim().notEmpty().withMessage(ERROR_CODE.code1)
    .if(check('password').notEmpty()).matches(regexPassword).withMessage(ERROR_CODE.code2)
        .custom((password, {req}) => {
            if(password !== req.body.password_confirm) {
                throw new Error('password and confirm password fields value must be matched')
            }
            return true;
        })
    ,registerController);