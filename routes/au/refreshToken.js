import express from 'express';
export const refreshTokenRoutes = express.Router();
import { refreshController } from '../../controllers/au/refreshTokenController.js';
import { check } from 'express-validator';
import { ERROR_CODE } from '../../config/error.js';
import { RESTURL } from '../../utils/constants.js';

// Refresh token
refreshTokenRoutes.post(RESTURL.PATH.REFRESH_TOKEN,
    check('refresh_token')
        .notEmpty()
        .withMessage(ERROR_CODE.code1)
        .if(check('refresh_token').notEmpty())
        .custom(value => {
            const elementJWT = value.split('.');
            if (elementJWT.length !== 3) {
                throw new Error("invalid value");
            }
            return true;
        }),
    refreshController);
