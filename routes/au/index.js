import express from 'express';
import {loginRoutes} from './login.js'
import {refreshTokenRoutes} from './refreshToken.js'
import {registerRoutes} from "./register.js";

const AURouter = express.Router();
AURouter.use(loginRoutes);
AURouter.use(refreshTokenRoutes);
AURouter.use(registerRoutes);

export default AURouter;

