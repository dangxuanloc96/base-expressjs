import express from 'express';
import { RESTURL } from '../utils/constants.js';
import AURouter  from '../routes/au/index.js'
import TESTRouter  from '../routes/test/index.js'

const router = express.Router();

router.use(`${RESTURL.VERSION.V1}${RESTURL.ROOT.AU}`, AURouter);
router.use(`${RESTURL.VERSION.V1}${RESTURL.ROOT.TEST}`, TESTRouter);

export default router;