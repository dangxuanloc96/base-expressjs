import mongoose from 'mongoose';
const UsersSchema = mongoose.Schema(
  {
    name: { type: String, required: true },
    phone: { type: String, required: true },
    email: { type: String, required: true },
    password:{ type: String, required: true },
    refresh_token:{ type: String, required: false },
  }
);
export default mongoose.model('users', UsersSchema);