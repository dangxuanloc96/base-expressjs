import jwt from 'jsonwebtoken';

// create and set expire date for token
// example expireDate
//expiresIn: "20d" // it will be expired after 20 days
//expiresIn: 120 // it will be expired after 120ms
//expiresIn: "120s" // it will be expired after 120s
//expiresIn: "10h" // it will be expired after 10h
const generateToken = (payload, expireTime = '3600s') =>
    jwt.sign({ ...payload }, process.env.JWT_SECRET, { expiresIn: expireTime });
export default generateToken;

