import moment from "moment";

export const RES_STATUS = {
    STATUS: {
        SUCCCESS: 200,
        ERROR: 400,
        NOT_FOUND: 404,
        NO_AUTH: 401,
        FORBIDDEN: 403,
        SERVER_ERROR: 500,
        SERVER_UNAVAILABLE: 503,
    },
    CODE: {
        CODE_XXX: 'xxx',
        CODE_1: '1',
        CODE_2: '2',
        CODE_100: '100',
        CODE_101: '101',
        CODE_103: '103',
        CODE_104: '104',
        CODE_100001: '100001',
        CODE_100002: '100002',
        CODE_100003: '100003',
        CODE_100004: '100004',
        CODE_1000000: '1000000',
    },
    MESSAGE: {
        BAD_REQUEST: 'Bad request',
        NOT_FOUND: 'Not found',
        NO_AUTH: 'Unauthorized',
        FORBIDDEN: 'Forbidden',
        SERVER_ERROR: 'Server Error',
        SERVER_UNAVAILABLE: 'Service Unavailable',
        CONSTRAINT_VIOLATION: 'Constraint violation',
    },
};

export const RESTURL = {
    VERSION: { V1: '/v1' },
    ROOT: { CM: '/cm', AO: '/ao', AF: '/af', CU: '/cu', CI: '/ci', AU: '/au', DM: '/dm', CV: '/cv', DB_UPDATE: '/db-update', TEST: '/test' },
    PATH: {
        LOGIN: "/login",
        REFRESH_TOKEN: "/refreshToken",
        REGISTER: "/register",
    }
};

export const pathLog = {
    errors: `./logs/errors/${moment().format("YYYY-MM-DD")}-errors.log`,
    warn: `./logs/warning/${moment().format("YYYY-MM-DD")}-warning.log`,
    info: `./logs/info/${moment().format("YYYY-MM-DD")}-info.log`,
    debug: `./logs/debug/${moment().format("YYYY-MM-DD")}-debug.log`
}