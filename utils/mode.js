import { MODE } from '../data/config/mode.js';
import { VALIDATION_ERROR_MODE } from '../data/config/validationErrorMode.js';
import { modeConfig } from '../config/mode.js';
import { validationErrorModeConfig } from '../config/validationErrorMode/index.js';
import { delay } from '../utils/utils.js';

export const validateMode = async (res, key) => {
    const mode = MODE[key];
    if (mode !== 0 && mode === 11) {
        // 故意にタイムアウトエラーを発生させる
        await delay(10000);
        res.status(500).send('タイムアウトエラー(意図的)');
        return false;
    }
    if (mode !== 0 && modeConfig[mode] !== undefined) {
        const { status } = modeConfig[mode];
        res.status(status).send(modeConfig[mode]);
        return false;
    }
    return true;
};

export const validateValidationErrorMode = (res, key) => {
    const mode = VALIDATION_ERROR_MODE[key];
    if (mode !== 0 && validationErrorModeConfig[key] !== undefined && validationErrorModeConfig[key][mode] !== undefined) {
        const { status } = validationErrorModeConfig[key][mode];
        res.status(status).send(validationErrorModeConfig[key][mode]);
        return false;
    }
    return true;
};
