import { RES_STATUS } from './constants.js';
import { ERROR_MESSAGE } from '../config/error.js';

export const delay = (ms = Math.floor(Math.random() * 500)) => new Promise((resolve) => setTimeout(resolve, ms));

/** This function is used for general handling of response */
const createResponse = (status, code, message, data = undefined) => {
    const response = {
        status: status,
        code: code,
        message: message,
    };
    return response;
};

const convertValidationErrArr = (errArr) => {
    return errArr.reduce((pre, cur) => {
        if (Object.keys(pre).includes(cur.param)) {
            // cur.msg is not message, this is message code.
            return {
                ...pre,
                [cur.param]: [...pre[cur.param], cur.msg],
            };
        } else {
            return {
                ...pre,
                [cur.param]: [cur.msg],
            };
        }
    }, {});
};

const DEFAULT_400_ERROR_RESPONSE = {
    status: RES_STATUS.STATUS.ERROR,
    code: RES_STATUS.CODE.CODE_100,
    message: RES_STATUS.MESSAGE.NOT_FOUND,
};

const createValidationErrorResponse = ({ code, data }) => {
    const convertedData = convertValidationErrArr(data);
    const details = Object.entries(convertedData).map(([param, codes]) => {
        return {
            target: param,
            details: codes.map((code) => ({
                code: code ?? '',
                message: ERROR_MESSAGE[code] ?? code,
            })),
        };
    });

    return {
        ...DEFAULT_400_ERROR_RESPONSE,
        code: code,
        message: RES_STATUS.MESSAGE.CONSTRAINT_VIOLATION,
        details,
    };
};

const create400Error = ({ code, data }) => {
    switch (code) {
        case RES_STATUS.CODE.CODE_100002:
            return {
                ...DEFAULT_400_ERROR_RESPONSE,
                code: RES_STATUS.CODE.CODE_100002,
                attributes: {
                    code: 'SMaoEAF0012',
                    messageTitle: '送付日は保管日より未来にはできません。',
                    messageTarget: 'エラーメッセージ(適宜使用)',
                    target: 'data[0].sendingDt;data[1].storageDt',
                },
            };
        case RES_STATUS.CODE.CODE_101:
            return createValidationErrorResponse({ code, data });
        case RES_STATUS.CODE.CODE_100003:
            return {
                ...DEFAULT_400_ERROR_RESPONSE,
                code: RES_STATUS.CODE.CODE_100003,
                attributes: {
                    code: 'SMaoEAF0012',
                    messageTitle: 'パラメータが不正です。',
                    messageTarget: 'エラーメッセージ(適宜使用)',
                },
            };
        case RES_STATUS.CODE.CODE_100001:
            return {
                ...DEFAULT_400_ERROR_RESPONSE,
                code: RES_STATUS.CODE.CODE_100001,
                attributes: {
                    code: 'SMaoEAF0012',
                    messageTitle: '予期せぬエラーが発生しました。',
                    messageTarget: 'エラーメッセージ(適宜使用)',
                },
            };
    }
};

const create5xxError = (code) => {
    switch (code) {
        case RES_STATUS.CODE.CODE_1:
            return {
                status: RES_STATUS.STATUS.SERVER_ERROR,
                code: RES_STATUS.CODE.CODE_1,
                message: RES_STATUS.MESSAGE.SERVER_ERROR,
            };

        case RES_STATUS.CODE.CODE_2:
            return {
                status: RES_STATUS.STATUS.SERVER_UNAVAILABLE,
                code: RES_STATUS.CODE.CODE_2,
                attributes: {
                    code: 'xxx',
                    messageTitle: 'メンテナンス',
                    messageTarget: 'ただいまメンテナンスのためにお使いただくことができません。',
                    retryable: '0',
                },
                message: RES_STATUS.MESSAGE.SERVER_UNAVAILABLE,
            };
    }
};

const createError403 = (status) => {
    return {
        status: status,
        code: RES_STATUS.CODE.CODE_100004,
        message: 'Forbidden',
        attributes: {
            code: 'SMaoEAF0012',
            messageTitle: 'ご指定のページはご利用できません。',
            messageTarget: 'エラーメッセージ(適宜使用)',
        },
    };
};

// eslint-disable-next-line complexity
export const buildResponseError = (status, code = undefined, data = []) => {
    switch (status) {
        case RES_STATUS.STATUS.ERROR:
            return create400Error({ code, data });
        case RES_STATUS.STATUS.NOT_FOUND:
            return createResponse(status, RES_STATUS.CODE.CODE_103, RES_STATUS.MESSAGE.NOT_FOUND);
        case RES_STATUS.STATUS.NO_AUTH:
            return createResponse(status, RES_STATUS.CODE.CODE_104, RES_STATUS.MESSAGE.NO_AUTH);
        case RES_STATUS.STATUS.FORBIDDEN:
            return createError403(status);
        case RES_STATUS.STATUS.SERVER_ERROR:
            return create5xxError(RES_STATUS.CODE.CODE_1);
        case RES_STATUS.STATUS.SERVER_UNAVAILABLE:
            return create5xxError(RES_STATUS.CODE.CODE_2);
        default:
            return {};
    }
};
const warning = [
    { id: 'SMaoWCI0001', message: 'ワーニング1' },
    { id: 'SMaoWCI0002', message: 'ワーニング2ワーニング2ワーニング2ワーニング2ワーニング2ワーニング2' },
    { id: 'SMaoWCI0002', message: 'ワーニング3' },
];
export const buildResponseSuccess = (data = undefined, message = warning) => {
    const response = {
        messages: message,
        data: data,
        status: RES_STATUS.STATUS.SUCCCESS
    };
    return response;
};
