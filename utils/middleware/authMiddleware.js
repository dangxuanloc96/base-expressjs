import jwt from 'jsonwebtoken';
import asyncHandler from 'express-async-handler';
import {buildResponseError} from "../utils.js";
import {RES_STATUS} from "../constants.js";


const sendError = (res) => {
    const responseError = buildResponseError(RES_STATUS.STATUS.NO_AUTH, RES_STATUS.CODE.CODE_104);
    return res.status(RES_STATUS.STATUS.NO_AUTH).send(responseError);
};

const auth = asyncHandler(async (req, res, next) => {
    let token;
    if (
        req.headers.authorization &&
        req.headers.authorization.startsWith('Bearer')
    ) {
        try {
            // eslint-disable-next-line prefer-destructuring
            token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.verify(token, process.env.JWT_SECRET);
            const {id: user_id, name, email, phone} = decoded;
            req.infoUser = {user_id, name, email, phone};
            next();
        } catch (error) {
            return sendError(res);
        }
    }
    if (!token) {
        return sendError(res);
    }
});

export {auth};
