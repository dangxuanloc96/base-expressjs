/* eslint-disable max-lines */
import url from 'url';
import { RES_STATUS } from '../constants.js';
import { buildResponseError } from '../utils.js';

const checkIsEmpty = (element) => {
    return !element || 0 === element.length;
};

// need header auth
const pathApiAuth = [
    '/v1/cu/applications/{ankenId}/threads/create',
    '/v1/cu/applications/{ankenId}/threads',
    '/v1/cu/threads/{threadId}/messages/list',
    '/v1/cu/threads/{threadId}',
    '/v1/cu/threads/{threadId}/update_status',
    '/v1/cu/threads/messages/{messageId}',
    '/v1/cu/threads/{threadId}/members',
    '/v1/dm/documents/{ankenId}',
    '/v1/dm/editDocument/{ankenId}',
    '/v1/dm/document/type/{ankenId}',
    '/v1/dm/documents/images',
    '/v1/dm/s3urls',
    '/v1/ci/applications/destinations',
    '/v1/ci/repayInfo/rates',
    '/v1/ci/repayInfo/ranges',
    '/v1/ci/repayInfo/repayments',
    '/v1/cv/property_collateral_values/',
    '/v1/ci/applications/stakeholders',
    '/v1/ci/applications/{ankenId}/list_versions',
    '/v1/ci/applications/{ankenId}/status',
    '/v1/cu/applications/{ankenId}/messages/create',
    '/v1/cu/messages/list',
    '/v1/af/customers',
    '/v1/af/customer_info/{userUlid}',
    '/v1/af/customers/{userUlid}',
    '/v1/ci/applications/{ankenId}/memos/search',
    '/v1/ci/applications/{ankenId}/memos/create',
    '/v1/ci/memos',
    '/v1/af/invitations/{invitationNumber}',
    '/v1/af/invitations/{ankenId}',
    '/v1/cm/forms',
    '/v1/cm/forms/list',
    '/v1/cu/deficiencies/{fubiGroupId}',
    '/v1/cu/deficiencies/{fubiId}',
    '/v1/af/email/post_request',
    '/v1/af/emailChanges/{userUlid}',
    '/v1/cu/deficiencyGroups/{ankenId}',
    '/v1/af/sales/create',
    '/v1/af/sales/{userUlid}',
    '/v1/af/sales/{userUlid}/get',
    '/v1/af/sales/search',
    '/v1/ci/pushNotices/mails',
    '/v1/ci/dicScreenings',
    '/v1/ci/applications/{ankenId}/indivshinsas/get_details',
    '/v1/ci/applications/{ankenId}/indivshinsas/list_attributes',
    '/v1/ci/applications/{ankenId}/indivshinsas/update_memo',
    '/v1/cm/masters',
    '/v1/dm/documentGenbutsus',
    '/v1/dm/documentGenbutsus/{genbutsuId}',
    '/v1/ci/queries/post',
    '/v1/ci/queries/{ankenQueryId}/get',
    '/v1/ci/queries/{ankenQueryId}/update/',
    '/v1/ci/queries/{ankenQueryId}/delete',
    '/v1/ci/queries/get_list',
    '/v1/af/agencies',
    '/v1/af/agencies/{agencyCompanyId}',
    '/v1/af/applications/agencies',
    '/v1/af/applications/agencies/delete',
    '/v1/cm/locks',
    '/v1/cm/locks/extend',
    '/v1/ci/applicationInfo/{ankenId}/histories',
    '/v1/ci/applications',
    '/v1/ci/applications/{ankenId}/copy',
    '/v1/ci/loanFinanceConds',
    '/v1/ci/loanFinanceConds/{ankenid}',
    '/v1/ci/application/search',
    '/v1/ci/applicationStatus/{ankenId}',
];

// need header cookie
const pathApiCookie = [
    '/v1/cu/applications/{ankenId}/threads/create',
    '/v1/cu/applications/{ankenId}/threads',
    '/v1/cu/threads/{threadId}/messages/list',
    '/v1/cu/threads/{threadId}',
    '/v1/cm/address',
    '/v1/dm/documents/history/{documentTypeId}',
    '/v1/dm/documents/images',
    '/v1/dm/s3urls',
    '/v1/ci/applications/sansan_ocr',
    '/v1/ci/qas/{tkHjnCd}/get',
    '/v1/ci/applications/{ankenId}/agreeStatus',
    '/v1/ao/customers/emails',
    '/v1/ao/bankingAccounts/{ankenId}',
    '/v1/ci/applications/{ankenId}/progress_status',
    '/v1/ci/applications/stakeholders',
    '/v1/cu/messages/list',
    '/v1/cu/message/{messageId}',
    '/v1/af/invitations/{invitationNumber}',
    '/v1/af/invitations/{ankenId}',
    '/v1/au/logout',
    '/v1/au/publicKeys',
    '/v1/af/users/identify/{invitationNumber}',
    '/v1/ao/pushNotifications',
    '/v1/cm/blockade',
    '/v1/cu/deficiencies/{fubiGroupId}',
    '/v1/cu/deficiencyGroups/{ankenId}',
    '/v1/ci/applications/{ankenId}/houchitasks/list',
    '/v1/ao/applications/{ankenId}/kouzas/nayose',
    '/v1/ao/kouzas/confirm',
    '/v1/cm/masters',
    '/v1/cm/locks',
    '/v1/cm/locks/extend',
    '/v1/af/customerSignup',
    '/v1/ci/applications/{ankenId}/apply',
    '/v1/ci/applications/{ankenId}/cancelRequests',
    '/v1/ci/applications/{ankenId}/copy',
    '/v1/ci/applicationDetails/{ankenId}',
    '/v1/af/meisiInfo/uploadToken',
];

const needAnkenId = [
    'AGE-001',
    'APP-004',
    'APP-005',
    'APP-006',
    'APP-007',
    'APP-008',
    'APP-013',
    'APP-015',
    'APP-016',
    'DOC-001',
    'DOC-002',
    'DOC-003',
    'DOC-009',
    'EXAM-001',
    'EXAM-002',
    'EXAM-003',
    'EXAM-004',
    'EXAM-006',
    'EXAM-007',
    'EXAM-008',
    'EXAM-009',
    'EXAM-010',
    'EXAM-012',
    'MEMO-001',
    'MEMO-003',
    'NEG-001',
    'NOTI-001',
    'NOTI-0013',
    'QA-001',
    'DEF-001',
    'LOAN-001',
    'CHT-0030',
    'CHT-0040',
    'CHT-0050',
    'CHT-0060',
];

const shouldCheckHeader = (array, pathname) => {
    const splitPathName = pathname.split('/');
    const checkPathInArr = array.find((item) => {
        const itemArr = item.split('/');
        const checkLength = itemArr.length === splitPathName.length;
        const checkValue = itemArr.every((value, index) => {
            if (value.includes('{')) {
                return true;
            }
            return value === splitPathName[index];
        });
        return checkValue && checkLength;
    });
    if (checkPathInArr) return true;
    return false;
};

// eslint-disable-next-line complexity
const validateHeader = (req, res, pathname, next) => {
    if (pathname.includes('db-update')) {
        return next();
    }
    // headers required common
    const userAgent = req.header('User-Agent');
    const checkUserAgent = checkIsEmpty(userAgent);
    const xAppId = req.header('X-Caller-App-Id');
    const checkUXAppId = checkIsEmpty(xAppId);
    const xKey = req.header('idempotency-key');
    const checkXKey = checkIsEmpty(xKey);
    const xType = req.header('X-Channel-Type');
    const checkXType = checkIsEmpty(xType);

    /** General error return function */
    const sendError = (res) => {
        const responseError = buildResponseError(RES_STATUS.STATUS.NO_AUTH, RES_STATUS.CODE.CODE_104);
        return res.status(RES_STATUS.STATUS.NO_AUTH).send(responseError);
    };

    /** According to common handling in yaml, post and put have 4 headers, get method has 3 headers */
    if (['POST', 'PUT', 'DELETE'].includes(req.method)) {
        if (checkUXAppId || checkXKey || checkXType || checkUserAgent) {
            return sendError(res);
        }
    }
    if (checkUXAppId || checkXType || checkUserAgent) {
        return sendError(res);
    }

    switch (xType) {
        case 'netbksmk_admin':
            if (shouldCheckHeader(pathApiAuth, pathname)) {
                const auth = req.header('Authorization');
                const checkAuthorization = checkIsEmpty(auth);
                if (checkAuthorization) {
                    return sendError(res);
                }
            }
            break;
        case 'netbksmk_web':
            if (shouldCheckHeader(pathApiCookie, pathname)) {
                const cookie = req.header('Cookie');
                const checkCookie = checkIsEmpty(cookie);
                if (checkCookie) {
                    return sendError(res);
                }
            }
            break;
        case 'netbksmk_app':
            break;
        default:
            return sendError(res);
    }

    // need X-Anken-Id in req.header
    if (needAnkenId.includes(xAppId)) {
        const xAnkenId = req.header('X-Anken-Id');
        if (checkIsEmpty(xAnkenId)) {
            return sendError(res);
        }
    }

    next();
};

export const headerApi = (req, res, next) => {
    const pathname = url.parse(req.url).pathname;
    validateHeader(req, res, pathname, next);
};
