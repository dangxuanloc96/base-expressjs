import winston from 'winston';
import  {pathLog} from '../utils/constants.js'

export default winston.createLogger({
    format: winston.format.combine(
        winston.format.splat(),
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.colorize(),
        winston.format.printf(
            log => {
                if(log.stack) return `[${log.timestamp}] [${log.level}] ${log.stack}`;
                return  `[${log.timestamp}] [${log.level}] ${log.message}`;
            },
        ),
    ),
    transports: [
        new winston.transports.File({
            level: 'error',
            filename: pathLog.errors
        }),
        new winston.transports.File({
            level: 'info',
            filename: pathLog.info
        }),
        new winston.transports.File({
            level: 'warn',
            filename: pathLog.warn
        }),
        new winston.transports.File({
            level: 'debug',
            filename: pathLog.debug
        }),
    ],
});
