export const modeConfig = {
    // 不明リソースエラー
    1: {
        status: 400,
        code: '103',
        attributes: {
            code: 'SMaoEAF0012',
            messageTitle: 'パラメータが不正です。',
        },
        message: 'Not found',
    },
    // システムエラー
    2: {
        status: 400,
        code: '100001',
        attributes: {
            code: 'SMaoEAF0012',
            messageTitle: 'エラー',
            messageTarget: '予期せぬエラーが発生しました。',
        },
        message: 'System Error',
    },
    // 認証エラー
    3: {
        status: 401,
        code: '1',
        message: 'Server error',
    },
    // 権限エラー
    4: {
        status: 403,
        code: '100004',
        attributes: {
            code: 'xxx',
            messageTitle: 'ご指定のページはご利用できません。',
        },
        message: 'Forbidden',
    },
    // NotFound
    5: {
        status: 404,
        code: '103',
        message: 'Not found',
    },
    // サーバ内部エラー（500）
    6: {
        status: 500,
        code: '1',
        message: 'Server Error',
    },
    // サーバ内部エラー（retryable 1）
    7: {
        status: 500,
        code: '1',
        message: 'Server Error',
        attributes: {
            retryable: '1',
        },
    },
    // サーバ利用不可エラー（500）
    8: {
        status: 500,
        code: '2015',
        message: 'API server error. API request is valid, but API server may be something wrong.',
        details: [
            {
                status: 503,
                code: '2020',
                attributes: {
                    apiName: 'netbk.sprinkle.sample.approval.v1.TravelCostService/SearchTravelCosts',
                },
                message: 'API (netbk.sprinkle.sample.approval.v1.TravelCostService/SearchTravelCosts) is under maintenance.',
            },
        ],
    },
    // サーバ利用不可エラー（503）
    9: {
        status: 503,
        code: '2020',
        message: 'API ({apiName}) is under maintenance.',
        attributes: {
            apiName: '/v1/approvals-with-count',
        },
    },
    // サーバ利用不可エラー（503）
    10: {
        status: 503,
        code: '2020',
        attributes: {
            code: 'xxxxx',
            messageTitle: 'サーバが利用できません。',
            retryable: '0',
        },
        message: 'Service Unavailable',
    },
    // タイムアウトエラー
    11: {},
};
