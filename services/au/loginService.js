import generateToken from '../../utils/generateToken.js';
import userModel from '../../models/users.js'
import bcrypt from 'bcrypt';

export const loginService = async (email, password) => {
    const user = await userModel.findOne({email: email});

    if(user !== null && bcrypt.compareSync(password, user.password)) {
        const payloadToken = {
            id: user._id,
            name: user.name,
            phone: user.phone,
            email: user.email
        };
        return  {access_token: generateToken(payloadToken), refresh_token: generateToken(payloadToken, '2d')}
    }

    throw new Error("Tài khoản không tồn tại trong hệ thống");
}