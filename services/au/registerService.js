import usersModel from '../../models/users.js';
import bcrypt from 'bcrypt';

export const registerUser = async (name, email, phone, password) => {
    const user = await usersModel.findOne({email: email});

    if (user) {
        throw new Error("Tài khoản đã tồn tại");
    }

    const newUser = new usersModel();
    newUser.name = name;
    newUser.email = email;
    newUser.phone = phone;
    newUser.password = await bcrypt.hashSync(password, Number.parseInt((process.env.BCRYPT_SALT_ROUND)));
    await usersModel.create(newUser);
}