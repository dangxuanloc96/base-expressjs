import usersModel from "../../models/users.js"
import jwt from "jsonwebtoken";
import generateToken from "../../utils/generateToken.js";

export const refreshTokenService = async (refreshToken) => {
    const decodedRefreshToken = jwt.verify(refreshToken, process.env.JWT_SECRET);
    const user = await usersModel.findOne({refresh_token: refreshToken});

    if (user == null || user._id !== decodedRefreshToken.id) {
        throw new Error("Refresh token không tồn tại trong hệ thống");
    }

    return  {access_token: generateToken(user._id), refresh_token: generateToken(user._id, '2d')};
}