import express from 'express';
import cookieParser from 'cookie-parser';
import routes from './routes/index.js';
import cors from 'cors';
import dotenv from 'dotenv';
import { headerApi } from './utils/middleware/middleware.js';
import { buildResponseError } from './utils/utils.js';
import { RES_STATUS } from './utils/constants.js';
import connectDb from './config/db.js';

dotenv.config();
connectDb();

// create our express app
const app = express();
/** delay timeout for app loading and
 Set the return result to no more than 1 seconds */
app.use((req, res, next) => {
    setTimeout(next, Math.floor(Math.random() * 1000));
});
// middleware
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
    cors({
        credentials: true,
        origin: ['http://localhost:3000', 'https://sumika.wize-dev.com', 'https://sumika-admin.wize-dev.com'],
    }),
);

app.use('/', routes);
app.all('*', (req, res) => {
    res.status(RES_STATUS.STATUS.NOT_FOUND).json(buildResponseError(RES_STATUS.STATUS.NOT_FOUND, RES_STATUS.CODE.CODE_103));
});
// start server
app.listen(4001, () => {
    console.log('listening at port:4001');
});

export default app;
