# base-expressjs



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Init project
```
cd existing_repo
git remote add origin https://gitlab.com/dangxuanloc96/base-expressjs.git
git branch -M main
git push -uf origin main
```

## Start project

```
npm install 
npm start
```

